## convention:
## all executables are in directory with this Makefile
## all non-executables & header files are in the include directory

.PHONY := all clean

CFILES := $(wildcard *.cc)
EFILES := $(addsuffix .e, $(basename $(CFILES)))
LIBFILES := $(wildcard include/*.cc)
OFILES := $(addsuffix .o, $(basename $(LIBFILES)))

all : include/lib.a $(EFILES)

## $^ means ALL of prerequisits
%.e : %.cc include/lib.a
	g++ -g -o $@ $^ -I include/

include/lib.a : $(OFILES)
	ar -r $@ $(OFILES)

include/%.o : include/%.cc
	g++ -g -c $^ -o $@

clean :
	rm -f $(OFILES) $(EFILES) include/lib.a

remake : clean
	$(MAKE)
