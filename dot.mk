DOTFILES = $(wildcard *.dot)
PSFILES = $(addsuffix .ps2, $(basename $(DOTFILES)))
PDFFILES = $(addsuffix .pdf, $(basename $(DOTFILES)))

graphs : $(PDFFILES)

%.pdf : %.ps2
	ps2pdf $^ $@

%.ps2 : %.dot
	dot -Tps2 $^ -o $@

graphs-clean :
	rm $(PDFFILES) $(PSFILES)
