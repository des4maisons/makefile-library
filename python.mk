RTUALENV=virtualenv27
ENVPATH=project-name-env
PYTHON=$(ENVPATH)/bin/python
PIP=$(ENVPATH)/bin/pip
PACKAGEPATH=deploy-server:release-dir/project_name
THIRDPARTYREPO=https://deploy-server/release-dir/project_name/thirdparty
PACKAGEREPO=https://deploy-server/release-dir/project_name
VERSION=

.PHONY: virtualenv develop install install-dev install-wheel package \
	thirdparty-wheels distribute-all distribute-package distribute-thirdparty \
	deploy-package deploy-all $(ENVPATH)

virtualenv: $(ENVPATH)
	$(VIRTUALENV) $(ENVPATH)

develop: virtualenv
	$(PIP) install -r requirements-dev.txt \

install: virtualenv
	$(PIP) install -r requirements.txt \
		. \
		--find-links=$(THIRDPARTYREPO) \
		--no-index

install-dev: virtualenv
	$(PIP) install -r requirements-dev.txt \
		--editable . \
		--find-links=$(THIRDPARTYREPO) \
		--no-index

install-wheel: virtualenv
	$(PIP) install wheel

package: virtualenv install-wheel
	$(PYTHON) setup.py bdist_wheel

thirdparty-wheels: virtualenv install-wheel
	$(PIP) wheel -r requirements.txt \
		--find-links=$(THIRDPARTYREPO)

distribute-all: distribute-package distribute-thirdparty

check-version:
	@VERSION=$${VERSION?'version of package must be set. See README.'}

distribute-package: check-version package
	scp dist/*.whl $(USER)@$(PACKAGEPATH)
	scp requirements.txt $(USER)@$(PACKAGEPATH)/requirements_$(VERSION).txt

distribute-thirdparty: thirdparty-wheels
	scp wheelhouse/*.whl $(USER)@$(PACKAGEPATH)/thirdparty/

deploy-package: check-version distribute-package
	sudo -u fresh -H /home/sandboxes/project-name/env/bin/pip install \
		project_name==$(VERSION) \
		--find-links=$(PACKAGEREPO) \
		--no-index
	sudo supervisorctl restart project-name

deploy-all: check-version distribute-all
	sudo -u fresh -H /home/sandboxes/project-name/env/bin/pip install \
		project_name==$(VERSION) \
		--find-links=$(PACKAGEREPO) \
		-r $(PACKAGEREPO)/requirements_$(VERSION) \
		--find-links=$(THIRDPARTYREPO) \
		--no-index
	sudo supervisorctl restart project-name

clean:
	rm -rf $(ENVPATH) dist wheelhouse build project_name.egg-info
